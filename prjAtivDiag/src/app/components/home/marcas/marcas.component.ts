import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.scss'],
})
export class MarcasComponent implements OnInit {

  Marcas = [
    {
      indice : 0,
      imagem : "../../../../assets/Marcas/Volkswagen.png",
    },
    {
      indice : 1,
      imagem : "../../../../assets/Marcas/fiat.png",
    },
    {
      indice : 0,
      imagem : "../../../../assets/Marcas/Volkswagen.png",
    },
    {
      indice : 1,
      imagem : "../../../../assets/Marcas/fiat.png",
    },
    {
      indice : 0,
      imagem : "../../../../assets/Marcas/Volkswagen.png",
    },
    {
      indice : 1,
      imagem : "../../../../assets/Marcas/fiat.png",
    },
    {
      indice : 0,
      imagem : "../../../../assets/Marcas/Volkswagen.png",
    },
    {
      indice : 1,
      imagem : "../../../../assets/Marcas/fiat.png",
    },
    {
      indice : 0,
      imagem : "../../../../assets/Marcas/Volkswagen.png",
    },
    {
      indice : 1,
      imagem : "../../../../assets/Marcas/fiat.png",
    },
    {
      indice : 0,
      imagem : "../../../../assets/Marcas/Volkswagen.png",
    },
    {
      indice : 1,
      imagem : "../../../../assets/Marcas/fiat.png",
    },
  ]


  constructor() { }

  option = {
    slidesPerView: 3,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10
  }


  ngOnInit() {}

}
