import { ChipsComponent } from './chips/chips.component';
import { PromocoesComponent } from './promocoes/promocoes.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { MarcasComponent } from './marcas/marcas.component';
import { CardAprendaComponent } from './card-aprenda/card-aprenda.component';
import { CardConcessionariaComponent } from './card-concessionaria/card-concessionaria.component';
import { CardComentarioComponent } from './card-comentario/card-comentario.component';
import { SlidesComponent } from './slides/slides.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],


  declarations: [HomePage,
     SlidesComponent,
     CardComentarioComponent,
     CardConcessionariaComponent,
     CardAprendaComponent,
     MarcasComponent,
     WelcomeComponent,
     PromocoesComponent,
     ChipsComponent
    ]
})
export class HomePageModule {}
